+++
title = "Aufgabenblatt 1" 
date = "2018-03-10T10:00:00+02:00" 
tags = ["Aufgabenblatt","Aufgabenblätter","Statusbericht","Projektplan","Risikoanalyse"] 
categories = ["Aufgabenblätter"]
banner = "/blog/images/James's_Flamingo_mating_ritual.jpg"
+++
## Aufgabenblatt 1

Abzugeben ist hier:

* [Projektplan](/blog/docs/projektplan.pdf)
* [Risikoanalyse](/blog/docs/risikoanalyse.pdf)
* [Statusbericht](/blog/docs/ab1/bericht_1.pdf)
* [Aufwandserfassung](/blog/docs/ab1/aufwandserfassung_k10.pdf)


----

