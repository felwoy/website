# How To be a cool motherfucker

Ich hab uns mal ne Möglichkeit geschaffen einfach Statusupdates auf unsere Homepages zu posten. Alles was ihr dafür tun müsst, ist eine Markdown-Datei mit gewünschtem Text in den **blog**-Ordner ablegen und das repo auf git pushen. Etwas warten, und die Seite liegt in aktualisierter Fassung vor. Zum Schreiben eurer Markdown Dati könnt z.B. https://stackedit.io/ verwenden. Beachtet hierbei, dass ihr das blogtemplate als Vorlage benutzt. 

# Aufbau Template
## Header
Am Anfang der Datei muss sich folgender Header befinden, damit die Webseite richtig aufgebaut werden kann. Dieser wird später auf der Seite nicht zu sehen sein.

+++ 

title = "Template"

date = "2018-03-08T22:22:22+02:00"

tags = ["Planung"]

categories = ["Template"]

banner = "/blog/example.png"

+++

Bei **titel** fügt ihr euren gewünschten Titel ein. Unter **date** kommt das Datum an dem Artikel veröffentlicht wird. **Achtung:** Wenn ihr ein zukünftiges Datum eingibt, wird der Artikel nicht veröffentlicht. Der wird erst veröffentlicht, wenn Jenkins, das nächste Mal die Seite updatet. Dies passiert alle 6 Stunden, oder ,wenn ein push auf dem Repo passiert.  
Unter **Tags** könnt ihr Tags definieren, anhand man später die Artikel in der Blog-Übersicht filtern kann. Unter **categories** definiert ihr die Kategorien, unter denen ein Artikel untergeordnet wird. **banner** legt das Bild von der Seite fest. **Wichtig** hierbei ist, dass das Bild sich auch im **blog**-Ordner befindet. In der Pfadangabe  muss **/blog/** vorangestellt werden (**/blog/**[yourImagePathHere])! Wenn ihr kein Bild habt, löscht einfach die banner-Zeile. 
# Dateien
Wenn ihr im Artikel weitere Dateien verlinken wollt, gilt hier das selbe Prinzip wie mit dem banner-Bild. Legt die Datei im **blog**-Ordner ab und erstellt im Text eine Verlinkung : [Dokument ](/blog/filePath)(**/blog/filePath**). Hierbei muss auch wieder **/blog/** vorangestellt werden.

Tobt euch einfach etwas aus. 
Spielt bitte nicht im **devenv**-Ordner rum und lasst die bitbucket-pipeline Datei in Ruhe. Achtet immer drauf, ihr Änderungen nut im blog-Ordner vornimmt. 
Wenn ihr Beispiele sehen wollt, wie man die Seite gestalten kann, schaut einfach mal hier rein: devenv/devsite/themes/hugo-universal-theme/exampleSite/content/blog
